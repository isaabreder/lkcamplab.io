---
title: "Round3"
date: 2019-03-26T19:55:15-03:00
draft: false
---

## Round 3 - Campinas/Brazil

### Mettings

- [M1: Welcome to LKCAMP!](https://docs.lkcamp.dev/unicamp_group/boot/)
    - When: 26/Mar/2019
    - Presenter: Helen Koike
    - [slides](https://gitlab.com/lkcamp/lkcamp_presentations_bin/-/blob/master/round3/lkcamp-M1.pdf)
    - [video](https://www.youtube.com/watch?v=BmBzNDzLi0w)

- [M2: First Contribution](https://docs.lkcamp.dev/unicamp_group/first_contrib/)
    - When: 02/Apr/2019
    - Presenters: Gabriel Mandaji / Danilo Rocha
    - [slides](https://gitlab.com/lkcamp/lkcamp_presentations_bin/-/blob/master/round3/lkcamp-M2.pdf)
    - [video](https://www.youtube.com/watch?v=VCwFkbxoY7I)

- [M3: Device Drivers](https://docs.lkcamp.dev/unicamp_group/dev_drivers/)
    - When: 09/Apr/2019
    - Presenters: Laís Pessine / Helen Koike
    - [slides](https://gitlab.com/lkcamp/lkcamp_presentations_bin/-/blob/master/round3/lkcamp-M3.pdf)
    - [video](https://www.youtube.com/watch?v=-ge-X0aUhJo)

- M4: How the system boots
    - When: 16/Apr/2019
    - Presenter: Lucas Magalhães
    - [slides](https://gitlab.com/lkcamp/lkcamp_presentations_bin/-/blob/master/round3/lkcamp-M4.pdf)
    - [video](https://www.youtube.com/watch?v=XQE_pizWV8c)

- [M5: System calls](https://docs.lkcamp.dev/unicamp_group/systemcalls/)
    - When: 23/Apr/2019
    - Presenter: Nícolas F. R. A. Prado
    - [slides](https://gitlab.com/lkcamp/lkcamp_presentations_bin/-/blob/master/round3/lkcamp-M5.pdf)
    - [video](https://www.youtube.com/watch?v=NCZiXmjYZHg)
