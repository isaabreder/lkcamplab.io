---
title: "Hackathon de Tradução"
date: 2022-09-08T00:13:31-03:00
draft: false
categories: ["Eventos"]
authors: ["Ícaro Chiabai"]
dataDoEvento: ["17","09","2022"]
---

Espaço para artes de divulgação, links de formulários, QRCodes, etc...

Obrigado por participar do Hackathon Tradução! Até a próxima :wink:
